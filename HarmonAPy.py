import datetime, hashlib, string, time, math, random, urlfetch, json

class HarmonAPy(object):
	def __init__(self, user, passwd, ssl):
		self.user = user
		self.passwd = passwd
		self.url = "http://api.harmony-hosting.com"
		if ssl:
			self.url = "https://api.harmony-hosting.com"

	def __token(self):
		date = datetime.datetime.now().isoformat()[:-7] + "+01:00"
		uuid = str(self.__uniqid('nonce_', True))
		nonce = hashlib.md5(uuid).hexdigest()[:16]
		nonceHigh = nonce.encode('base64')[:-1]
		passDigestSha = hashlib.sha1(nonce + date + self.passwd).digest()
		passDigest = passDigestSha.encode('base64')[:-1]
		token = "UsernameToken Username=\""+self.user+"\", PasswordDigest=\""+passDigest+"\", Nonce=\""+nonceHigh+"\", Created=\""+date+"\""
		
		return token

	def __uniqid(self, prefix='', more_entropy=False):
		m = time.time()
		uniqid = '%8x%05x' %(math.floor(m),(m-math.floor(m))*1000000)
		if more_entropy:
			valid_chars = list(set(string.hexdigits.lower()))
			entropy_string = ''
			for i in range(0,10,1):
				entropy_string += random.choice(valid_chars)
				uniqid = uniqid + entropy_string
		
		uniqid = prefix + uniqid
		return uniqid

	def get(self, url, headers={}):
		url = self.url + url
		headers['Content-type'] = 'application/json'
		headers['X-WSSE'] = self.__token()

		result = urlfetch.get(url, headers=headers)

		if result.status >= 400:
			return "Error: HTTP Status Code " + str(result.status) + " : " + result.reason

		try:
			rep = json.loads(result.body)
		except Exception, e:
			rep = result.body

		return rep

	def put(self, url, data=None, headers={}):
		url = self.url + url
		headers['Content-type'] = 'application/json'
		headers['X-WSSE'] = self.__token()
		data = urlfetch.urlencode(data)

		result = urlfetch.put(url, headers=headers, params=data)

		if result.status >= 400:
			return "Error: HTTP Status Code " + str(result.status) + " : " + result.reason

		try:
			rep = json.loads(result.body)
		except Exception, e:
			rep = result.body

		return rep

	def post(self, url, data=None, headers={}):
		url = self.url + url
		headers['Content-type'] = 'application/json'
		headers['X-WSSE'] = self.__token()

		result = urlfetch.post(url, headers=headers, params=data)

		if result.status >= 400:
			return "Error: HTTP Status Code " + str(result.status) + " : " + result.reason

		try:
			rep = json.loads(result.body)
		except Exception, e:
			rep = result.body

		return rep

	def delete(self, url, data=None, headers={}):
		url = self.url + url
		headers['Content-type'] = 'application/json'
		headers['X-WSSE'] = self.__token()

		result = urlfetch.delete(url, headers=headers, params=data)

		if result.status >= 400:
			return "Error: HTTP Status Code " + str(result.status) + " : " + result.reason

		try:
			rep = json.loads(result.body)
		except Exception, e:
			rep = result.body

		return rep