# HarmonAPy

## How to use
#### First import the lib
```
import HarmonAPy as api
```

#### Then init the class
```
api = HarmonAPy.HarmonAPy("user", "pass", ssl)
```
The user is your H-H username,
pass is your API password from H-H
and ssl is True is you want to use it.

#### Then you can finally make your request
```
req = api.get("/version")
print req
```

#### Whick should output something like
```
{u'version': 0.1}
```

## List of available functions

#### Get
```
api.get(url='/url', headers={'Variable': 'Value'})
```
Send a GET request to url with headers
(token headers are automatically added)

#### Post
```
api.post(url='/url', data={'Variable': 'Value'}, headers={'Variable': 'Value'})
```
Send a POST request to url with data and headers
(token headers are automatically added)

#### Put
```
api.put(url='/url', data={'Variable': 'Value'}, headers={'Variable': 'Value'})
```
Send a PUT request to url with data and headers
(token headers are automatically added)

#### Delete
```
api.delete(url='/url', data={'Variable': 'Value'}, headers={'Variable': 'Value'})
```
Send a DELETE request to url with data and headers
(token headers are automatically added)